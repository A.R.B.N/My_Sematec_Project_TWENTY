package ir.arbn.www.mysematecprojecttwenty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    WebView myWeb;
    MyEditText url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findViewById(R.id.show).setOnClickListener(this);
        myWeb = findViewById(R.id.myWeb);
        url = findViewById(R.id.url);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show)
            openWeb(url.text());
    }

    void openWeb(String url) {
        myWeb.getSettings().setJavaScriptEnabled(true);
        myWeb.setWebViewClient(new WebViewClient());
        if (url.startsWith("http://") || url.startsWith("https://")) {
        } else {
            url = "http://" + url;
        }
        myWeb.loadUrl(url);
    }
}